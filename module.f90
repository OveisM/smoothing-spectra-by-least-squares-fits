module least_square_module
    implicit none
    
    integer, parameter :: r=4   ! r=4 for single precision, r=8 for double precision
    real, parameter :: pi=3.14159265359
    contains

    function mean(vector, size, weight)
        ! ---------------------------------------------------------------------
        ! Description:
        !   This function calculates the weighted/unweighted mean value of a vector
        !   according the formula:
        !   mean = (Σ(weight(i) * vector(i))) / size

        ! Parameters:
        !   vector (input)   - The input vector of real numbers.
        !   size (input)     - The size of the input vector.
        !   weight (input)   - The weight vector, specifying the weight of each element in the input vector.

        ! Returns:
        !   mean (output)    - The weighted mean of the input vector.
        
        ! Implicit Inputs:
        !   r - The kind parameter for real numbers.
        ! ---------------------------------------------------------------------
        implicit none
        integer, intent(in) :: size
        real(kind=r), dimension(size), intent(in) :: vector, weight
        real(kind=r) :: sum, mean
        integer :: i

        sum = 0
        do i=1, size
            sum = sum + weight(i)*vector(i)
        enddo
        mean = sum / float(size)
        return

    end function mean

    function determinant(matrix)
        ! ---------------------------------------------------------------------
        ! Description:
        !   This function calculates the determinant of a 3x3 matrix
        
        ! Parameters:
        !   matrix (input)   - The input matrix of real numbers.
        
        ! Returns: 
        !   determinant (output) - The determinant of the input matrix.
        ! 
        ! Implicit Inputs:
        !   r - The kind parameter for real numbers.
        ! ---------------------------------------------------------------------
        implicit none
        real(kind=r), intent(in), dimension(3,3) :: matrix
        real(kind=r) :: determinant
            
        determinant = matrix(1,1)*matrix(2,2)*matrix(3,3) &
                    + matrix(1,2)*matrix(2,3)*matrix(3,1) &
                    + matrix(1,3)*matrix(2,1)*matrix(3,2) &
                    - matrix(1,3)*matrix(2,2)*matrix(3,1) &
                    - matrix(1,2)*matrix(2,1)*matrix(3,3) &
                    - matrix(1,1)*matrix(2,3)*matrix(3,2)

    end function determinant

    subroutine coefficients(omega_mean, sigma, A0, A1, A2, omega_max, gamma, S)
        ! ---------------------------------------------------------------------
        ! Description:
        !   This subroutine calculates and returns the characteristics of the 
        !   spectral line (S, gamma and omega_max coefficients) according to 
        !   the formula:
        !   omega_max = omega_mean - sigma*A1/(2*A2)
        !   gamma = sigma * sqrt(A0/A2 - A1*A1/(4*A2*A2))
        !   S = pi*sigma / sqrt(A0*A2 - A1*A1/4.)
        
        ! Parameters:
        !   omega_mean (input)  - The mean value of the input vector.
        !   sigma (input)       - The standard deviation of the input vector.
        !   A0 (input)          - The A0 coefficient of the polynomial.
        !   A1 (input)          - The A1 coefficient of the polynomial.
        !   A2 (input)          - The A2 coefficient of the polynomial.
        
        ! Returns:
        !   omega_max (output)  - The omega_max coefficient of the spectral line.
        !   gamma (output)      - The gamma coefficient of the spectral line
        !   S (output)          - The S coefficient of the spectral line.
        ! --------------------------------------------------------------------- 
        implicit none
        real(kind=r), intent(in) :: omega_mean, sigma, A0, A1, A2
        real(kind=r), intent(out):: omega_max, gamma, S
        
        omega_max = omega_mean - sigma*A1/(2.*A2)
        gamma     = sigma * sqrt(A0/A2 - A1*A1/(4.*A2*A2))
        S         = pi*sigma / sqrt(A0*A2 - A1*A1/4.)
        return
    end subroutine coefficients

    subroutine intensity_theo(size, S, gamma, omega, omega_max, intensity)
        ! ---------------------------------------------------------------------
        ! Description:
        !   This subroutine calculates the theoretical values of the intensity
        ! Parameters:
        !   size (input)        - The size of the input vector.
        !   S (input)           - The S coefficient of the spectral line.
        !   gamma (input)       - The gamma coefficient of the spectral line
        !   omega (input)       - The input vector of real numbers.
        !   omega_max (input)   - The omega_max coefficient of the spectral line.
        !
        ! Returns:
        !   intensity (output)  - The output vector of real numbers.
        ! ---------------------------------------------------------------------
        implicit none
        integer, intent(in) :: size
        real(kind=r), intent(in) :: S, gamma, omega_max
        real(kind=r), dimension(size), intent(in) :: omega
        real(kind=r), dimension(size), intent(out) :: intensity
        integer :: i

        do i=1, size
            intensity(i) = S*gamma / (pi * ((omega(i)-omega_max)&
                           *(omega(i)-omega_max) + gamma*gamma ))
        enddo

        return
    end subroutine intensity_theo
end module