import numpy as np
import matplotlib.pyplot as plt

# Making a list containing 5 arrays, each includs the data from a spectrum file
List = []
for i in range(5):
    List.append(np.loadtxt("./data%i.txt"%(i+1), skiprows=1)[:,:])

# Making 3 lists, containing the related values from all the files
omega=[]     # A list of 5 arrays, each containing the wavenumbers of a spectrum (1, 3, 6, 10 and 15 atm)
F_exp=[]     # A list of 5 arrays, each containing the experimental intencities of a spectrum
F_theo_w=[]  # A list of 5 arrays, each containing the theoretical intencities of a spectrum
F_theo=[]
for i in range(5):
    omega.append(List[i][:,0])
    F_exp.append(List[i][:,1])
    F_theo_w.append(List[i][:,3])
    F_theo.append(List[i][:,2])

#################################################################################
#      Experimental and theoretical intencities of the spectrum in P=1 atm      #
#################################################################################
# fig1 = plt.figure()

plt.plot(omega[0], F_theo[0]   , "--" , color="y"   , label='Theo. (W=1)')
plt.plot(omega[0], F_theo_w[0] , "-"  , color="b"   , label='Theo. (W=${F_{exp}} ^2$)')
plt.plot(omega[0], F_exp[0]    , "."  , markersize=2, color="r", label='Exp. intencies')
plt.title('Spectrum 2: 3 atm'      , fontsize=20)
plt.xlabel('Wavenumber ($cm^{-1}$)', fontsize=16)
plt.ylabel('Intencity, arb. units' , fontsize=16)
plt.xlim(2280,2286)
plt.ylim(0,60)
plt.legend()
plt.grid()
plt.show()


#################################################################################
#      Experimental and theoretical intencities of the spectrum in P=1 atm      #
#################################################################################
# fig2 = plt.figure()
plt.plot(omega[0], F_exp[0], ".", markersize=1.5, color="r")
plt.plot(omega[0], F_theo_w[0]  , color="r", label='1 atm')
plt.plot(omega[1], F_exp[1], ".", markersize=1.5, color="g")
plt.plot(omega[1], F_theo_w[1]  , color="g", label='3 atm')
plt.plot(omega[2], F_exp[2], ".", markersize=1.5, color="b")
plt.plot(omega[2], F_theo_w[2]  , color="b", label='6 atm')
plt.plot(omega[3], F_exp[3], ".", markersize=1.5, color="c")
plt.plot(omega[3], F_theo_w[3]  , color="c", label='10 atm')
plt.plot(omega[4], F_exp[4], ".", markersize=1.5, color="y")
plt.plot(omega[4], F_theo_w[4]  , color="y", label='15 atm')

plt.title('Fits of spectra in 5 different pressures', fontsize=20)
plt.xlabel('Wavenumber, $cm^{-1}$', fontsize=16)
plt.ylabel('Intencity, arb. units', fontsize=16)

plt.xlim(2280,2288)
plt.legend()
plt.grid()
plt.show()

##########################################################
#       Linear regression of the maximum positions       #
##########################################################
# fig3 = plt.figure()

Omega_m = np.loadtxt("./data0.txt", skiprows=1)[:,1]
Pressure = np.array([1,3,6,10,15])

plt.scatter(Pressure, Omega_m, color ="b")         # adding the points on the graph

Linear_Model = np.polyfit(Pressure, Omega_m, 1)    # Finding the best fit (a and b in f(x)=ax*b)
Linear_Model_func = np.poly1d(Linear_Model)       #  making a function from the best fit 

x = np.arange(0,17)
plt.plot(x, Linear_Model_func(x), color="r")

plt.title('Linear regression of the maximum position', fontsize=20)
plt.xlabel('Pressure (atm)', fontsize=14)
plt.ylabel('$\omega_{m}$ ($cm^{-1}$)', fontsize=14)

plt.text(0.5,2285.7, "$\omega_{0}=$ %.4f $cm^{-1}$"%Linear_Model[1], color="g", fontsize=12)
plt.text(0.5,2285.5, "$\Delta\omega=$ %f $cm^{-1}$"%Linear_Model[0], color="g", fontsize=12)

plt.text(0.5 , Omega_m[0]+0.2 , "1 atm"  , color="b" , fontsize=12)
plt.text(2.5 , Omega_m[1]+0.2 , "3 atm"  , color="b" , fontsize=12)
plt.text(5.5 , Omega_m[2]+0.2 , "6 atm"  , color="b" , fontsize=12)
plt.text(9.5 , Omega_m[3]-0.3 , "10 atm" , color="b" , fontsize=12)
plt.text(14.5, Omega_m[4]-0.3 , "15 atm" , color="b" , fontsize=12)

plt.xlim(0,18)
plt.ylim(2282,2286)
plt.grid()
plt.show()

####################################################
#       Linear regression of the line-widths       #
####################################################
# fig4 = plt.figure()

Gamma = np.loadtxt("./data0.txt", skiprows=1)[:,3]

plt.scatter(Pressure, Gamma, color ="b")         # adding the points on the graph

Linear_Model = np.polyfit(Pressure, Gamma, 1)    # Finding the best fit (a and b in f(x)=ax*b)
Linear_Model_func = np.poly1d(Linear_Model)        # 

x = np.arange(0,16)
plt.plot(x, Linear_Model_func(x), color="r")

plt.text(0.5,3.1, "$\gamma_{1}=$ %f"%Linear_Model[0], color="g", fontsize=12)
plt.text(0.5 , Gamma[0]+0.2 , "1 atm"  , color="b" , fontsize=12)
plt.text(2.5 , Gamma[1]+0.2 , "3 atm"  , color="b" , fontsize=12)
plt.text(5.5 , Gamma[2]+0.2 , "6 atm"  , color="b" , fontsize=12)
plt.text(9.5 , Gamma[3]-0.3 , "10 atm" , color="b" , fontsize=12)
plt.text(14.5, Gamma[4]-0.3 , "15 atm" , color="b" , fontsize=12)

plt.title('Linear regression of the line-widths', fontsize=20)
plt.xlabel('Pressure (atm)', fontsize=16)
plt.ylabel('Line-width', fontsize=16)

plt.xlim(0,17)
plt.ylim(0,3.5)
plt.grid()
plt.show()
