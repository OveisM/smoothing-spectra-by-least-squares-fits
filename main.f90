
program main
    use iso_fortran_env
    use least_square_module
    ! -------------------------------------------------------------------------
    ! Description:
    ! This program reads the experimental intensities from 5 different samples
    ! and calculates the characteristic parameters of the spectra. It also
    ! calculates the theoretical intensities and stores the wavenumbers, 
    ! experimental and theoretical intensities for all spectra in the output 
    ! files.

    ! Parameters:
    !   h:            Number of spectra
    !   omega:        Wavenumbers
    !   omega2:       Square of wavenumbers
    !   x:            Normalized wavenumbers
    !   x2:           Square of normalized wavenumbers
    !   x3:           Cube of normalized wavenumbers
    !   x4:           Fourth power of normalized wavenumbers
    !   xy:           Product of normalized wavenumbers and experimental 
    !                 intensities
    !   x2y:          Product of square of normalized wavenumbers and 
    !                 experimental intensities
    !   weight1:      Weight=1
    !   F_exp:        Experimental intensities
    !   y:            Inverse of experimental intensities
    !   F_theo:       Theoretical intensities
    !   F_theo_w:     Theoretical intensities with weight=F(exp)^2
    !   omega_mean:   Average of wavenumbers
    !   omega2_mean:  Average of square of wavenumbers
    !   sigma:        Standard deviation of wavenumbers
    !   weight_mean:  Average of weight=F(exp)^2
    !   denum_det:    Determinant of denuminator matrix
    !   x_mean:       Average of normalized wavenumbers
    !   x2_mean:      Average of square of normalized wavenumbers
    !   x3_mean:      Average of cube of normalized wavenumbers
    !   x4_mean:      Average of fourth power of normalized wavenumbers
    !   y_mean:       Average of inverse of experimental intensities
    !   xy_mean:      Average of product of normalized wavenumbers and 
    !                 experimental intensities
    !   x2y_mean:     Average of product of square of normalized wavenumbers 
    !                 and experimental intensities
    !   a0, a1, a2, omega_max, gamma, S: Characteristic parameters
    !   omega_max_w:  Omega_max with weight=F(exp)^2
    !   gamma_w:      Gamma with weight=F(exp)^2
    !   S_w:          S with weight=F(exp)^2
    !   a0_numerator: Numerator matrix of a0
    !   a1_numerator: Numerator matrix of a1
    !   a2_numerator: Numerator matrix of a2
    !   denuminator:  Denuminator matrix of a0, a1, and a2
    !   line_num:     Number of data points in each spectrum
    !   n1 to n6:     Character variables used to define names of the I/O files
    !   io:           I/O status
    ! -------------------------------------------------------------------------
    
    implicit none
    integer, parameter :: h=5 ! Number of spectra
    real(kind=r), dimension(1000) :: omega, omega2, x, x2, x3, x4, xy, x2y,&
                                     weight1, weight
    real(kind=r), dimension(h,1000) :: F_exp, y, F_theo, F_theo_w
    real(kind=r) :: omega_mean, omega2_mean, sigma, weight_mean, denum_det,&
                    x_mean, x2_mean, x3_mean, x4_mean, y_mean, xy_mean, x2y_mean,&
                    a0, a1, a2, omega_max, gamma, S, omega_max_w, gamma_w, S_w
    real(kind=r), dimension(3,3) :: a0_numerator, a1_numerator, a2_numerator, denuminator
    integer, dimension(h) :: line_num
    character(len=*), parameter :: n1 = 'spectre_'
    character(len=*), parameter :: n3 = '.txt'
    character(len=1) :: n2
    character(len=4) :: n5
    character(len=13):: n4
    character(len=9) :: n6
    integer :: i, j, io

! ---------------------------------------------------------------
! |      Read the experimental values from the input files      |
! ---------------------------------------------------------------
    line_num(:)=0

    do i=1,h
        write(n2,'(i1)') i
            n4=n1//n2//n3
            open(unit=10,file=n4,status='old',action='read')
            do
                ! The value assigned to the variable passed to the 
                ! IOSTAT= specifier of an I/O statement if an end-of-file 
                ! condition occurred.
                read(10, *, iostat = io)
                if (io == iostat_end) exit    
                line_num(i) = line_num(i) + 1
            enddo
            rewind(unit=10)
            ! Calculation of the inverse of experimental intensities
            do j=1, line_num(i)
                read(10, *, iostat=io) F_exp(i,j)
                y(i,j) = 1./F_exp(i,j)
            enddo
            close (unit=10)
    enddo

    open(unit=15,file="data0.txt")      ! The three characteristics are stored in data0.txt
    write(15,'(6A15)') "Omega_max", "Omega_max_w", "Gamma", "Gamma_w","S", "S_w"

! -------------------------------------------------------
! |      Main loop to iterate over all h(=5) spectra      |
! -------------------------------------------------------
    do i=1, h
        print*,""
        print*, "--------------------------------------------------------"
        print*, "Spectrum:", i, "Data number:", line_num(i)
        print*, "--------------------------------------------------------"
        ! ------------------------------------------------------------------
        ! |      Making the wavenumbers and the square of wavenumbers      |
        ! ------------------------------------------------------------------
        do j=1, line_num(i)
            omega(j)  = 2280 + (j-1)*0.01
            omega2(j) = omega(j)*omega(j)
        enddo
        weight1(1:line_num(i)) = 1.0
        ! ------------------------------------------------------------
        ! |      Calculation of sigma, x, x2, x3, x4, xy, x^2 y      |
        ! ------------------------------------------------------------
        omega_mean  = mean(omega,  line_num(i), weight1)
        omega2_mean = mean(omega2, line_num(i), weight1)
        sigma       = sqrt(omega2_mean - omega_mean*omega_mean)
        ! print "('sigma = ',F10.8)", sigma

        do j=1, line_num(i)
            x(j)   = (omega(j) - omega_mean) / sigma
            x2(j)  = x(j)*x(j)
            x3(j)  = x2(j)*x(j)
            x4(j)  = x3(j)*x(j)
            xy(j)  = y(i,j)*x(j)
            x2y(j) = xy(j)*x(j)
        enddo

        ! ----------------------------------------
        ! |      Calculations with weight=1      |
        ! ----------------------------------------
        ! Averages of x, x2, x3, x4, xy, x^2y and y
        x_mean   = mean(x,  line_num(i), weight1)
        x2_mean  = mean(x2, line_num(i), weight1)
        x3_mean  = mean(x3, line_num(i), weight1)
        x4_mean  = mean(x4, line_num(i), weight1)
        y_mean   = mean(y(i,:), line_num(i), weight1)
        xy_mean  = mean(xy, line_num(i), weight1)
        x2y_mean = mean(x2y,line_num(i), weight1)
        ! print "('x_mean = ',F12.8)", x_mean

        ! Numerator and denuminator matrices of a0, a1, and a2
        a0_numerator = reshape([y_mean,  xy_mean, x2y_mean, &
                                x_mean,  x2_mean, x3_mean , &
                                x2_mean, x3_mean, x4_mean], [3,3])
        
        a1_numerator = reshape([1.,      x_mean,  x2_mean,  &
                                y_mean,  xy_mean, x2y_mean, &
                                x2_mean, x3_mean, x4_mean], [3,3])
            
        a2_numerator = reshape([1.,     x_mean,  x2_mean,   &
                                x_mean, x2_mean, x3_mean,   &
                                y_mean, xy_mean, x2y_mean], [3,3])
        
        denuminator  = reshape( [1.,     x_mean,  x2_mean,  &
                                x_mean,  x2_mean, x3_mean,  &
                                x2_mean, x3_mean, x4_mean], [3,3])
        
        ! Unweighted a0, a1, and a2
        denum_det = determinant(denuminator)

        a0 = determinant(a0_numerator) / denum_det
        a1 = determinant(a1_numerator) / denum_det
        a2 = determinant(a2_numerator) / denum_det

        ! Unweighted characteristic parameters
        call coefficients(omega_mean, sigma, a0, a1, a2, omega_max, gamma, S)
        
        ! -----------------------------------------------
        ! |      Calculations with weight=F(exp)^2      |
        ! -----------------------------------------------
        do j=1, line_num(i)
            weight(j) = F_exp(i,j)*F_exp(i,j)
        enddo
        
        ! Averages of x, x2, x3, x4, xy, x^2y and y
        x_mean   = mean(x,  line_num(i), weight)
        x2_mean  = mean(x2, line_num(i), weight)
        x3_mean  = mean(x3, line_num(i), weight)
        x4_mean  = mean(x4, line_num(i), weight)
        y_mean   = mean(y(i,:),  line_num(i), weight)
        xy_mean  = mean(xy, line_num(i), weight)
        x2y_mean = mean(x2y,line_num(i), weight)
        weight_mean = sum(weight)/float(line_num(i))
        ! print*, weight_mean
        
        ! Numerator and denuminator matrices of a0, a1, and a2
        a0_numerator = reshape([y_mean,  xy_mean, x2y_mean, &
                                x_mean,  x2_mean, x3_mean , &
                                x2_mean, x3_mean, x4_mean], [3,3])
        
        a1_numerator = reshape([weight_mean, x_mean,  x2_mean,  &
                                y_mean,      xy_mean, x2y_mean, &
                                x2_mean,     x3_mean, x4_mean], [3,3])
            
        a2_numerator = reshape([weight_mean, x_mean,  x2_mean,   &
                                x_mean,      x2_mean, x3_mean,   &
                                y_mean,      xy_mean, x2y_mean], [3,3])
        
        denuminator  = reshape([weight_mean, x_mean,  x2_mean,  &
                                x_mean,      x2_mean, x3_mean,  &
                                x2_mean,     x3_mean, x4_mean], [3,3])

        ! Weighted a0, a1, and a2
        denum_det = determinant(denuminator)
        a0 = determinant(a0_numerator) / denum_det
        a1 = determinant(a1_numerator) / denum_det
        a2 = determinant(a2_numerator) / denum_det
        ! print "('x_mean = ',F12.8)", x_mean

        ! Weighted characteristic parameters
        call coefficients(omega_mean, sigma, a0, a1, a2, omega_max_w, gamma_w, S_w)

        print *, "Omega_max   =", omega_max
        print *, "Omega_max_w =", omega_max_w

        print *, "Gamma   =", gamma
        print *, "Gamma_w =", gamma_w

        print *, "S   =", S
        print *, "S_w =", S_w
        
        write(15,'(6F16.8)') omega_max, omega_max_w, gamma, gamma_w, S, S_w

        ! ----------------------------------------------------------------
        ! |      Theoretical intensity values (weighted/unweighted)      |
        ! ----------------------------------------------------------------
        call intensity_theo(line_num(i), S  , gamma  , omega, omega_max  , F_theo(i,:))
        call intensity_theo(line_num(i), S_w, gamma_w, omega, omega_max_w, F_theo_w(i,:))

    enddo
    close(15)
    ! Saving wavenumbers and the experimental and theoretical intensities in
    ! the output files
    n5 = 'data'
    do i=1,h
        write(n2,'(i1)') i
        n6=n5//n2//n3
        open(unit=12,file=n6)
        write(12,'(4A20)') "Omega", "F_Exp", "F_Theo", "F_Theo_Weighted"
        do j=1, line_num(i)
            write(12,'(4F20.8)') omega(j), F_exp(i,j), F_theo(i,j), F_theo_w(i,j)
        enddo
        close(12)
    enddo

end program main























