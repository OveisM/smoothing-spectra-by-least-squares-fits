# Smoothing Spectra by Least-Squares Fits

## Description
In this project the least-squares method is used to fit the experimental spectra of a gas in five different pressures. In order to make the numerical calculation a FORTRAN90 code is written. Performing linear regressions with the results of the numerical calculation of the least square fits we were able to find the line-width and broadening coefficient.

## Support
Do not hesitate to contact me if you have any questions, find any mistakes or any possible way of improving the codes.
Email: oveis.mahmoudi@gmail.com

## License
The spectra data files are provided by the course instructor, Dr. Jeanna Buldyreva (jeanna.buldyreva@univ-fcomte.fr). The codes are developed by Oveis Mahmoudi and are free to use and modify.

## Project status
This project is done in December 2021 as the practical work for the "Numerical Methods 1" course, in the master program of Computational Physics, at Université de Franche-Comté, Besançon, France.